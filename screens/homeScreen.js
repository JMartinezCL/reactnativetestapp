import * as React from 'react';
import { Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import styles from '../styles/style';
import Card from '../components/Card';

const HomeScreen = ({route, navigation}) =>{

	const Users = () => {
		const cards = route.params?.dataUsers.map((item) => (
			<Card key={item.id}>
				<Text 
					key={item.id} 
					style={{textAlign:'center', fontSize:18, paddingBottom:5}}
				>
					{item.name}
				</Text>
				<TouchableOpacity  
					style={style.customButton} 
					onPress={() => navigation.navigate('Details',{
						character: item,
					})}
				>
					<Text 
						style={{color:'white', width:100, textAlign:'center'}}
					>
						Go to Details
					</Text>
				</TouchableOpacity>
			</Card>
		));		
		return (cards);
	}

	return(
		<ScrollView>       
			<Users />
		</ScrollView>
	);
}

const style = StyleSheet.create({
	customButton:styles.customButton
});

export default HomeScreen;