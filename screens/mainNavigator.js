import * as React from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from  "react-native-splash-screen";
import HomeScreen from '../screens/homeScreen';
import DetailsScreen from '../screens/detailsScreen';

const Stack = createStackNavigator();

function MainNavigator(){
	return(
		<NavigationContainer>
			<Stack.Navigator initialRouteName="Home">
				<Stack.Screen 
					name="Home" component={HomeScreen} 
					options={{
						title: 'Book List', 
						headerStyle:{backgroundColor:'#4D13C1'},
						headerTintColor:'white'
					}}
				/>
				<Stack.Screen 
					name="Details" component={DetailsScreen}
					options={{
						title: 'Details', 
						headerStyle:{backgroundColor:'#4D13C1'},
						headerTintColor:'white'
					}}
				/>
			</Stack.Navigator>
		</NavigationContainer>
	);
}

export default MainNavigator;