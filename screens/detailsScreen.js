import React from 'react';
import { Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import styles from '../styles/style';
import Card from '../components/Card';

const DetailsScreen = ({ route, navigation }) => {
	
	const { character } = route.params;	

	return (	
		<Card>
			<Text style={{fontWeight:'bold', fontSize:18}}>{character.name}</Text>
			<Text>{character.type}</Text>
			<Image 
				style={{width: 300, height: 300, marginBottom:5}} 
				source={character.image} 
			/>
			<TouchableOpacity style={style.customButton} onPress={() => navigation.goBack()}>
				<Text style={{ color: 'white', width: 100, textAlign: 'center' }}>Go back</Text>
			</TouchableOpacity>
		</Card>		
	);
}

const style = StyleSheet.create({
	customButton: styles.customButton
});

export default DetailsScreen;