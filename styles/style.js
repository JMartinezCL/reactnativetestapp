const styles = {
    customButton: {
      elevation: 8,
      backgroundColor: "#4D13C1",
      borderRadius: 20,
      paddingVertical: 10,
      paddingHorizontal: 12,
      alignItems: 'center',
      width:150,      
    },
    card: {
      borderRadius: 6,
      elevation: 3,
      backgroundColor: '#fff',
      shadowOffset: { width: 1, height: 1 },
      shadowColor: '#333',
      shadowOpacity: 0.3,
      shadowRadius: 2,
      marginHorizontal: 4,
      marginVertical: 6,
    },
    cardContent: {
      marginHorizontal:18,
      marginVertical: 10,
      alignItems:'center'
    }
};

export default styles;