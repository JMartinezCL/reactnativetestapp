import React from 'react';
import { StyleSheet, View} from 'react-native';
import styles from '../styles/style';

const Card = (props) => {
	return (
		<View style={style.card}>
			<View style={style.cardContent}>
				{props.children}
			</View>
		</View>
	);
}

const style = StyleSheet.create({
	card: styles.card,
	cardContent: styles.cardContent,
});
export default Card;