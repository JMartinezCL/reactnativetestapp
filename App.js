import React, {useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from  "react-native-splash-screen";

import HomeScreen from './screens/homeScreen';
import DetailsScreen from './screens/detailsScreen';
import {dataUsers} from './Api/Api';

const Stack = createStackNavigator();

const App = () =>{

  useEffect(() => {    
    SplashScreen.hide();    
  },[]);

  return(
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home" component={HomeScreen}
          initialParams={{dataUsers}}
          options={{
            title: 'DC List', 
            headerStyle:{backgroundColor:'#4D13C1'},
            headerTintColor:'white'
          }}
        />
        <Stack.Screen 
          name="Details" component={DetailsScreen}          
          options={{
            title: 'Details', 
            headerStyle:{backgroundColor:'#4D13C1'},
            headerTintColor:'white'
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;