# DC List

An application for first aproach with **React Native**.
Features in the demo includes:
- splash screen.
- navigation screen to screen.
- share data between screen.

## Getting Started
- Download zip project or clone git repository on your local workspace.
- Connect mobile device to your computer, or start emulated device.
- Execute ***npm install***.
- On terminal execute ***npm run start***.
- On a second terminal execute ***npm run android*** (android environment).

Used Packages:
- react-navigation

## ScreenShoots

<p align="center">
  <img src="assets/screenshoots/1.jpg" width="200" alt="1"/>
  <img src="assets/screenshoots/2.jpg" width="200" alt="2"/> 
  <img src="assets/screenshoots/3.jpg" width="200" alt="3"/>  
</p>


<p align="center">
  <img src="assets/screenshoots/4.jpg" width="200" alt="4"/>
  <img src="assets/screenshoots/5.jpg" width="200" alt="5"/>
  <img src="assets/screenshoots/6.gif" width="200" alt="6"/>
</p>

# Authors
* **José Martínez** - [JMartinezCL](https://gitlab.com/JMartinezCL)
